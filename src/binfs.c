/*
** binfs.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <atucos/vfs.h>
#include <atucos/app.h>

struct vfs_inode *bin_root;

static void binfs_add_entry(const char *id, void *addr)
{
    struct vfs_inode *bin_entry = vfs_create_inode(VFS_EXEC, NULL);
    bin_entry->content.data = addr;
    bin_root->content.dir = vfs_create_dir(id, bin_entry, bin_root->content.dir);
}

void binfs_init(void)
{
    struct vfs_inode *root = vfs_getroot();
    bin_root = vfs_create_inode(VFS_DIR, root);
    struct vfs_dir *bin_dir = vfs_create_dir("bin", bin_root, root->content.dir);
    root->content.dir = bin_dir;
    binfs_add_entry("hello", app_hello);
    binfs_add_entry("game", app_game);
}
