/*
** process.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <stdlib.h>
#include <string.h>
#include <sdramc.h>
#include <atucos/dev.h>
#include <atucos/process.h>
#include <atucos/mem.h>
#include <atucos/log.h>

struct process *current_process = NULL;

void process_set_context(struct process *proc, struct context *context)
{
    memcpy(&proc->context, context, sizeof (struct context));
    asm volatile ("stmts --sp, sp" "\n\t"
                  "ld.w  %0, sp++"
                  : "=r" (proc->sp));
}

struct process *process_create(const char *id,
                               uint32_t pc,
                               struct dev *stdin,
                               struct dev *stdout,
                               struct dev *stderr,
                               struct dev *stdcom,
                               struct vfs_inode *wd)
{
    struct process *proc = malloc(sizeof (struct process));
    strncpy(proc->id, id, PROC_NAME_LEN);
    proc->status = PROC_STATUS_NEW;
    proc->fd = malloc(sizeof (struct dev *) * 4);
    proc->fd_size = 4;
    proc->fd_maxsize = 4;
    proc->fd[0] = stdin;
    proc->fd[1] = stdout;
    proc->fd[2] = stderr;
    proc->fd[3] = stdcom;
    proc->wd = wd;
    proc->builtin = 0;
    mem_process(proc);
    if (current_process == NULL)
    {
        proc->next = proc;
        proc->prev = proc;
        current_process = proc;
    }
    else
    {
        proc->next = current_process->next;
        proc->prev = current_process;
        current_process->next->prev = proc;
        current_process->next = proc;
    }
    proc->context.pc = pc;
    proc->context.sr = 0;
    proc->context.lr = 0;
    proc->context.r12 = 0;
    proc->context.r11 = 0;
    proc->context.r10 = 0;
    proc->context.r9 = 0;
    proc->context.r8 = 0;
    return proc;
}

void process_destroy(void)
{
    struct process *cp = current_process;
    if (cp->next != cp)
    {
        cp->next->prev = cp->prev;
        cp->prev->next = cp->next;
        current_process = cp->next;
    }
    else
        current_process = NULL;
    free(cp->fd);
    free(cp);
}

void proc_run(struct process *proc)
{
    current_process = proc;
    klog("Starting %s\r\n"
         "\tregion = %d\r\n"
         "\tframes = %x\r\n"
         "\tentrypoint = %x\r\n"
         "\tstack = %x\r\n",
            proc->id, proc->region, proc->frames, proc->pc, proc->sp);
    mem_setmpu(proc);
    asm volatile ("st.w  --sp, %0"       "\n\r"
                  "mfsr  r8, 0"          "\n\r"
                  "andh  r8, 0xFE3F"     "\n\r"
                  "st.w  --sp, r8"       "\n\r"
                  "st.w  --sp, %1"       "\n\r"
                  "ldmts sp++, sp"       "\n\r"
                  "rets"
                  :
                  : "r" (proc->pc),
                    "r" (proc->sp));
}
