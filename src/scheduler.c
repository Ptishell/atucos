/*
** sheduler.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <avr32/io.h>
#include <string.h>
#include <intc.h>
#include <board.h>
#include <sysclk.h>
#include <tc.h>
#include <atucos/log.h>
#include <atucos/mem.h>
#include <atucos/process.h>

#define TIMER           (&AVR32_TC)
#define TIMER_CHANNEL   1
#define TIMER_IRQ_GROUP AVR32_TC_IRQ_GROUP
#define TIMER_IRQ       AVR32_TC_IRQ1

static void tc_irq_handler(void)
{
    struct context *context;
    tc_read_sr(TIMER, TIMER_CHANNEL);
    asm volatile ("mov %0, sp" "\r\n"
                  "mov r8, 12" "\r\n"
                  "add %0, r8"
                  : "=r" (context)
                  :
                  : "r8");
    if (current_process->status != PROC_STATUS_NEW)
        process_set_context(current_process, context);
    else
        current_process->status = PROC_STATUS_RUNNING;
    if (current_process->status == PROC_STATUS_EXITED)
        process_destroy();
    else
        current_process = current_process->next;
    mem_setmpu(current_process);
    memcpy(context, &current_process->context, sizeof (struct context));
    asm volatile ("st.w  --sp, %0"       "\n\r"
                  "ldmts sp++, sp"       "\n\r"
                  :
                  : "r" (current_process->sp));
}

static void init_tc(volatile avr32_tc_t *tc)
{
    // Options for waveform genration.
    static const tc_waveform_opt_t waveform_opt = {
        // Channel selection.
        .channel  = TIMER_CHANNEL,
        // Software trigger effect on TIOB.
        .bswtrg   = TC_EVT_EFFECT_NOOP,
        // External event effect on TIOB.
        .beevt    = TC_EVT_EFFECT_NOOP,
        // RC compare effect on TIOB.
        .bcpc     = TC_EVT_EFFECT_NOOP,
        // RB compare effect on TIOB.
        .bcpb     = TC_EVT_EFFECT_NOOP,
        // Software trigger effect on TIOA.
        .aswtrg   = TC_EVT_EFFECT_NOOP,
        // External event effect on TIOA.
        .aeevt    = TC_EVT_EFFECT_NOOP,
        // RC compare effect on TIOA.
        .acpc     = TC_EVT_EFFECT_NOOP,
        /* RA compare effect on TIOA.
         * (other possibilities are none, set and clear).
         */
        .acpa     = TC_EVT_EFFECT_NOOP,
        /* Waveform selection: Up mode with automatic trigger(reset)
         * on RC compare.
         */
        .wavsel   = TC_WAVEFORM_SEL_UP_MODE_RC_TRIGGER,
        // External event trigger enable.
        .enetrg   = false,
        // External event selection.
        .eevt     = 0,
        // External event edge selection.
        .eevtedg  = TC_SEL_NO_EDGE,
        // Counter disable when RC compare.
        .cpcdis   = false,
        // Counter clock stopped with RC compare.
        .cpcstop  = false,
        // Burst signal selection.
        .burst    = false,
        // Clock inversion.
        .clki     = false,
        // Internal source clock 3, connected to fPBA / 8.
        .tcclks   = TC_CLOCK_SOURCE_TC3
    };

    // Options for enabling TC interrupts
    static const tc_interrupt_t tc_interrupt = {
        .etrgs = 0,
        .ldrbs = 0,
        .ldras = 0,
        .cpcs  = 1, // Enable interrupt on RC compare alone
        .cpbs  = 0,
        .cpas  = 0,
        .lovrs = 0,
        .covfs = 0
    };
    // Initialize the timer/counter.
    tc_init_waveform(tc, &waveform_opt);

    /*
     * Set the compare triggers.
     * We configure it to count every 10 milliseconds.
     * We want: (1 / (fPBA / 8)) * RC = 10 ms, hence RC = (fPBA / 8) / 100
     * to get an interrupt every 10 ms.
     */
    tc_write_rc(tc, TIMER_CHANNEL, (sysclk_get_pba_hz() / 8 / 100));
    // configure the timer interrupt
    tc_configure_interrupts(tc, TIMER_CHANNEL, &tc_interrupt);
    // Start the timer/counter.
    tc_start(tc, TIMER_CHANNEL);
}

void scheduler_init(void)
{
    static const tc_interrupt_t tc_interrupt =
    {
        .etrgs = 0,
        .ldrbs = 0,
        .ldras = 1,
        .cpcs  = 0,
        .cpbs  = 0,
        .cpas  = 0,
        .lovrs = 0,
        .covfs = 0
    };
    klog("[... ] Setting up scheduler");
    INTC_register_interrupt(&tc_irq_handler, TIMER_IRQ, AVR32_INTC_INT3);

    init_tc(TIMER);
    klog("\r[OK  ]\r\n");
}
