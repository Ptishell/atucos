/*
** env.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Mar 17 09:07:05 2014 Pierre Surply
** Last update Tue Mar 18 15:28:23 2014 Pierre Surply
*/

#include <stdlib.h>
#include <string.h>
#include <atucos/env.h>
#include <atucos/log.h>

struct env
{
    char *id;
    char *value;
    struct env *next;
};

static struct env *env = NULL;

void addenv(const char *id, const char *value)
{
    struct env *e;

    for (e = env; e; e = e->next)
    {
        if (strcmp(id, e->id) == 0)
        {
            free(e->value);
            e->value = strdup(value);
            return;
        }
    }

    e = malloc(sizeof (struct env));
    e->id = strdup(id);
    e->value = strdup(value);
    e->next = env;
    env = e;
}

void delenv(const char *id)
{
    struct env *e;
    struct env *prev = NULL;

    for (e = env; e; e = e->next)
    {
        if (strcmp(id, e->id) == 0)
        {
            if (prev)
                prev->next = e->next;
            else
                env = e->next;
            free(e->id);
            free(e->value);
            free(e);
            return;
        }
        prev = e;
    }
}

char *getenv(const char *id)
{
    struct env *e;

    for (e = env; e; e = e->next)
    {
        if (strcmp(id, e->id) == 0)
            return e->value;
    }

    return "";
}

void env_init(void)
{
    klog("[... ] Setting up environment variables");

#define ENV_VAR(I, V)                           \
    addenv(I, V)

    DEFAULT_ENV;

#undef ENV_VAR

    klog("\r[OK  ]\r\n");
}

void cmd_printenv(char *arg[])
{
    struct env *e;

    for (e = env; e; e = e->next)
    {
        if (!arg[1] || strcmp(arg[1], e->id) == 0)
            klog("%s=%s\n\r", e->id, e->value);
    }
}

void cmd_setenv(char *arg[])
{
    if (!arg[1])
        return;
    if (arg[2])
        addenv(arg[1], arg[2]);
    else
        delenv(arg[1]);
}
