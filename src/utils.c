#include <stdlib.h>
#include <stdint.h>

void delay(uint32_t d)
{
    for (; d; --d)
        continue;
}
