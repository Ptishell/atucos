/*
** dev.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <string.h>
#include <stdlib.h>
#include <avr32/io.h>
#include <atucos/dev.h>
#include <atucos/vfs.h>
#include <atucos/drv/null.h>
#include <atucos/drv/zero.h>
#include <atucos/drv/random.h>
#include <atucos/drv/lcd.h>
#include <atucos/drv/sdmmc.h>
#include <atucos/drv/eth.h>
#include <atucos/drv/usart.h>
#include <atucos/drv/joystick.h>
#include <atucos/drv/leds.h>
#include <atucos/drv/buttons.h>

struct vfs_inode *dev_root;

static struct dev *devfs_create_dev(const char *id,
                                    volatile void *addr,
                                    struct drv *drv)
{
    struct dev *dev = malloc(sizeof (struct dev));
    strncpy(dev->id, id, DEV_NAME_LEN);
    dev->addr = addr;
    dev->drv = drv;
    return dev;
}

static struct vfs_inode *devfs_create_dev_entry(const char *id,
                                                volatile void *addr,
                                                struct drv *drv,
                                                struct dev **dev)
{
    *dev = devfs_create_dev(id, addr, drv);
    struct vfs_inode *dev_entry = vfs_create_inode(VFS_DEV, NULL);
    dev_entry->content.dev = *dev;
    return dev_entry;
}

static void devfs_add_entry(const char *id, volatile void *addr,
                            struct drv *drv)
{
    struct dev *dev;
    dev_root->content.dir = vfs_create_dir(id,
            devfs_create_dev_entry(id, addr, drv, &dev),
            dev_root->content.dir);
    drv->init(dev);
}

void devfs_init(void)
{
    struct vfs_inode *root = vfs_getroot();
    dev_root = vfs_create_inode(VFS_DIR, root);
    struct vfs_dir *dev_dir = vfs_create_dir("dev", dev_root, root->content.dir);
    root->content.dir = dev_dir;
    devfs_add_entry("null", NULL, &drv_null);
    devfs_add_entry("zero", NULL, &drv_zero);
    devfs_add_entry("random", NULL, &drv_random);
    devfs_add_entry("sdmmc", NULL, &drv_sdmmc);
    devfs_add_entry("lcd", NULL, &drv_lcd);
    devfs_add_entry("eth", NULL, &drv_eth);
    devfs_add_entry("usart0", &AVR32_USART0, &drv_usart);
    devfs_add_entry("usart1", &AVR32_USART1, &drv_usart);
    devfs_add_entry("usart2", &AVR32_USART2, &drv_usart);
    devfs_add_entry("usart3", &AVR32_USART3, &drv_usart);
    devfs_add_entry("joystick", NULL, &drv_joystick);
    devfs_add_entry("leds", NULL, &drv_leds);
    devfs_add_entry("buttons", NULL, &drv_buttons);
}
