/*
** syscall.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <stdlib.h>
#include <stdint.h>
#include <atucos/drv.h>
#include <atucos/dev.h>
#include <atucos/process.h>
#include <atucos/log.h>

int sys_write(int fd, char *buff, size_t len)
{
    return current_process->fd[fd]->drv->write(current_process->fd[fd],
                                               buff, len);
}

int sys_read(int fd, char *buff, size_t len)
{
    return current_process->fd[fd]->drv->read(current_process->fd[fd],
                                              buff, len);
}

int sys_open(char *path)
{
    struct vfs_inode *file = vfs_getinode(current_process->wd, path);
    if (!file)
        return -1;
    if (current_process->fd_size >= current_process->fd_maxsize)
    {
        current_process->fd_maxsize *= 2;
        current_process->fd = realloc(current_process->fd,
                current_process->fd_maxsize);
    }
    current_process->fd[current_process->fd_size] = file->content.dev;
    return current_process->fd_size++;
}

int sys_exec(char *cmd[], int fd[])
{
    struct vfs_inode *file = vfs_getinode(current_process->wd, cmd[0]);
    if (file && file->type == VFS_EXEC)
    {
        struct process *proc = process_create(cmd[0], file->content.data,
                current_process->fd[0],
                current_process->fd[1],
                current_process->fd[2],
                current_process->fd[3],
                current_process->wd);
        proc->builtin = 1;
        return 0;
    }
    return -1;
}

int sys_exit(int retval)
{
    klog("%s exited with %d\r\n", current_process->id, retval);
    current_process->status = PROC_STATUS_EXITED;
}

int sys_ioctl(int fd, int op, void *arg)
{
    return current_process->fd[fd]->drv->ioctl(current_process->fd[fd],
                                               op, arg);
}

static int (*syscalls[])() =
{
    sys_write,
    sys_read,
    sys_open,
    sys_exec,
    sys_exit,
    sys_ioctl,
    NULL
};

int scall_handler(int syscall, void *arg1, void *arg2,
                   void *arg3, void *arg4)
{
    if (syscall <= sizeof (syscalls) / sizeof(syscalls[0]))
        return syscalls[syscall](arg1, arg2, arg3, arg4);
    return -1;
}
