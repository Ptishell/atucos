/*
** vfs.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <string.h>
#include <atucos/vfs.h>
#include <atucos/log.h>

static struct vfs_inode vfs;

struct vfs_dir *vfs_create_dir(const char *id,
                               struct vfs_inode *inode,
                               struct vfs_dir *dir)
{
    struct vfs_dir *d = malloc(sizeof (struct vfs_dir));
    strncpy(d->id, id, VFS_ID_LEN);
    d->inode = inode;
    d->next = dir;
    return d;
}

struct vfs_inode *vfs_create_inode(enum vfs_node_type type,
                                   struct vfs_inode *parent)
{
    struct vfs_inode *inode = malloc(sizeof (struct vfs_inode));
    inode->type = type;
    if (type == VFS_DIR)
    {
        inode->content.dir = vfs_create_dir(".", inode, NULL);
        inode->content.dir = vfs_create_dir("..", parent, inode->content.dir);
    }
    return inode;
}

void vfs_init(void)
{
    klog("[... ] Setting up environment variables");
    vfs.type = VFS_DIR;
    vfs.content.dir = vfs_create_dir(".", &vfs, NULL);
    vfs.content.dir = vfs_create_dir("..", &vfs, vfs.content.dir);
    klog("\r[OK  ]\r\n");
}

struct vfs_inode *vfs_getroot(void)
{
    return &vfs;
}

struct vfs_inode *vfs_getsub(struct vfs_dir *dir, const char *name, size_t len)
{
    for (; dir; dir = dir->next)
    {
        if (strncmp(dir->id, name, len) == 0)
            return dir->inode;
    }
    return NULL;
}

struct vfs_inode *vfs_getinode(struct vfs_inode *wd, const char *path)
{
    int i = 0;
    int j;

    if (path == NULL)
        return NULL;
    if (path[i] == '/')
    {
        wd = vfs_getroot();
        ++i;
    }
    for (; path[i]; ++i)
    {
        j = i;
        for (; path[i] && path[i] != '/'; ++i)
            continue;
        if (wd->type == VFS_DIR)
            wd = vfs_getsub(wd->content.dir, path + j, i - j);
        if (!path[i])
            return wd;
    }
    return wd;
}
