/*
** mem.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <stdlib.h>
#include <sdramc.h>
#include <mpu.h>
#include <atucos/log.h>
#include <atucos/process.h>

#define NB_CONFIG_MPU_ENTRIES       4
#define REGION_INTRAM               0
#define REGION_FLASH                1
#define REGION_DEV                  2
#define REGION_USER                 3

#define REGION_SIZE 65536
#define REGIONS     512
#define FRAME_SIZE  4096
#define NB_FRAMES   16

static uint8_t regions[REGIONS];
static mpu_entry_t dmpu_entries[NB_CONFIG_MPU_ENTRIES];

void mem_init(void)
{
    int i;
    klog("[... ] Setting up Memory Management");
    for (i = 0; i < REGIONS; ++i)
        regions[i] = 0;

    dmpu_entries[REGION_INTRAM].addr = 0;
    dmpu_entries[REGION_INTRAM].size = MPU_REGION_SIZE_64KB;
    dmpu_entries[REGION_INTRAM].valid = 1;
    set_mpu_entry(&dmpu_entries[REGION_INTRAM], REGION_INTRAM);
    set_access_permissions(REGION_INTRAM, MPU_APRA_ID, MPU_PRIVRW_UNPRIVNONE);
    set_access_permissions(REGION_INTRAM, MPU_APRB_ID, MPU_PRIVRW_UNPRIVRW);
    select_subregion(REGION_INTRAM, 0);

    dmpu_entries[REGION_FLASH].addr = AVR32_FLASH_ADDRESS;
    dmpu_entries[REGION_FLASH].size = MPU_REGION_SIZE_512KB;
    dmpu_entries[REGION_FLASH].valid = 1;
    set_mpu_entry(&dmpu_entries[REGION_FLASH], REGION_FLASH);
    set_access_permissions(REGION_FLASH, MPU_APRA_ID, MPU_PRIVRX_UNPRIVNONE);
    set_access_permissions(REGION_FLASH, MPU_APRB_ID, MPU_PRIVRX_UNPRIVRX);
    select_subregion(REGION_FLASH, 0);

    dmpu_entries[REGION_DEV].addr = 0xFFFE0000;
    dmpu_entries[REGION_DEV].size = MPU_REGION_SIZE_128KB;
    dmpu_entries[REGION_DEV].valid = 1;
    set_mpu_entry(&dmpu_entries[REGION_DEV], REGION_DEV);
    set_access_permissions(REGION_DEV, MPU_APRA_ID, MPU_PRIVRW_UNPRIVNONE);
    select_subregion(REGION_DEV, 0);

    enable_mpu();
    klog("\r[OK  ]\r\n");
}

void mem_setmpu(struct process *proc)
{
    const uint16_t perms = proc->builtin ? 0xFFFF : 0;

    disable_mpu();

    select_subregion(REGION_FLASH, perms);
    select_subregion(REGION_INTRAM, perms);

    dmpu_entries[REGION_USER].addr = SDRAM + proc->region * REGION_SIZE;
    dmpu_entries[REGION_USER].size = MPU_REGION_SIZE_64KB;
    dmpu_entries[REGION_USER].valid = 1;
    set_mpu_entry(&dmpu_entries[REGION_USER], REGION_USER);
    set_access_permissions(REGION_USER, MPU_APRA_ID, MPU_PRIVRW_UNPRIVRW);
    set_access_permissions(REGION_USER, MPU_APRB_ID, MPU_PRIVRW_UNPRIVRX);
    select_subregion(REGION_USER, proc->frames);

    enable_mpu();
}

int mem_process(struct process *proc)
{
    int i;

    for (i = 0; i < REGIONS; ++i)
    {
        if (regions[i] == 0)
        {
            regions[i] = 1;
            proc->region = i;
            proc->frames = 0;
            proc->sp = i * REGION_SIZE + SDRAM + REGION_SIZE;
            return 0;
        }
    }
    return -1;
}
