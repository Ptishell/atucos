/*
** interrupt.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <stdint.h>
#include <stdlib.h>
#include <avr32/io.h>
#include <intc.h>
#include <atucos/log.h>
#include <atucos/process.h>

extern void *evba;

void interrupt_init(void)
{
    klog("[... ] Setting up interrupts");
    INTC_init_interrupts();
    klog("\r[OK  ]\r\n");
}

void segfault(uint32_t addr)
{
    klog("Segmentation fault: %s at %x", current_process->id, addr);
    for (;;)
        continue;
}

void unrecoverable_exception(void)
{
    panic("Unrecoverable exception\n\r");
}

void bad_interrupt(void)
{
    panic("Bad Interrupt\n\r");
}

void bus_error_data_handler(void)
{
    panic("Bus error data fetch\n\r");
}

void coprocessor_absent(void)
{
    panic("Coprocessor absent\n\r");
}

void data_address_write_handler(void)
{
    panic("Data address (Write)\n\r");
}

void itlb_protection_handler(void)
{
    panic("ITLB protection\n\r");
}

void illegal_opcode(void)
{
    panic("Illegal opcode\n\r");
}
