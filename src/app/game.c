#include <atucos.h>
#include <atucos/drv/lcd.h>
#include <atucos/drv/joystick.h>
#include <stdint.h>

#define WIDTH 20
#define HEIGHT 4

#define SCREEN_SIZE (WIDTH * HEIGHT)

struct pos
{
    uint8_t x;
    uint8_t y;
};

static struct
{
    struct pos pos;
    uint8_t lifes;
} player;

static void refresh_screen(int lcd)
{
    char screen[SCREEN_SIZE];
    int x, y;

    for (y = 0; y < HEIGHT; ++y)
    {
        for (x = 0; x < WIDTH; ++x)
            screen[x + y * WIDTH] = ' ';
    }

    screen[player.pos.x + player.pos.y * WIDTH] = '>';

    ioctl(lcd, IOCTL_LCD_CLEAR, NULL);
    write(lcd, screen, SCREEN_SIZE);
}

static void handle_input(char input_joystick)
{
    if (IS_JOYSTICK_UP(input_joystick) && player.pos.y > 0)
        --player.pos.y;
    else if (IS_JOYSTICK_DOWN(input_joystick) && player.pos.y < HEIGHT - 1)
        ++player.pos.y;

    if (IS_JOYSTICK_LEFT(input_joystick) && player.pos.x > 0)
        --player.pos.x;
    else if (IS_JOYSTICK_RIGHT(input_joystick) && player.pos.x < WIDTH - 1)
        ++player.pos.x;
}

static void set_leds(int leds)
{
    const char val = (1 << player.lifes) - 1;
    write(leds, &val, 1);
}

void app_game(void)
{
    int lcd = open("/dev/lcd");
    int joystick = open("/dev/joystick");
    int leds = open("/dev/leds");
    char input_joystick;

    ioctl(lcd, IOCTL_LCD_HIDE_CURSOR, NULL);
    write(lcd, "Hello", 5);

    player.pos.x = 0;
    player.pos.y = 1;
    player.lifes = 5;

    set_leds(leds);

    while (1)
    {
        read(joystick, &input_joystick, 1);
        if (input_joystick != 0)
        {
            handle_input(input_joystick);
            refresh_screen(lcd);
            while (input_joystick != 0)
                read(joystick, &input_joystick, 1);
        }
    }

    for (;;)
        continue;
}
