#include <atucos.h>

void app_hello(void)
{
    int lcd = open("/dev/lcd");

    write(lcd, "Hello World !", 13);
    write(STDOUT_FILENO, "Hello World !\r\n", 15);

    _exit(0);
}
