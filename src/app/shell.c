/*
** shell.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <string.h>
#include <atucos.h>

struct cmd
{
    const char *id;
    void (*f)(char *arg[]);
    const char *info;
    const char *usage;
};

void app_main(char *arg[]);
void cmd_help(char *arg[]);
void cmd_echo(char *arg[]);
void cmd_printenv(char *arg[]);
void cmd_setenv(char *arg[]);
void cmd_reset(char *argv[]);
void cmd_run(char *argv[]);
void cmd_ls(char *argv[]);
void cmd_cd(char *argv[]);

static struct cmd cmd[] =
{
    {"setenv", cmd_setenv, "set environment variables", "name value"},
    {"printenv", cmd_printenv, "print environment variables", "[name]"},
    {"run", cmd_run, "run commands in an environment variable", "name"},
    {"reset", cmd_reset, "perform reset of the CPU", ""},
    {"echo", cmd_echo, "echo args to console", "[args]..."},
    //{"ls", cmd_ls, "list current directory", "[args]..."},
    //{"cd", cmd_cd, "change working directory", "dir"},
    {"help", cmd_help, "show help", "[cmd]"},
    {NULL, NULL, NULL, NULL}
};

void cmd_echo(char *arg[])
{
    int i;

    for (i = 1; arg[i]; ++i)
    {
        write(STDOUT_FILENO, "[", 1);
        write(STDOUT_FILENO, arg[i], strlen(arg[i]));
        write(STDOUT_FILENO, "]", 1);
    }
    write(STDOUT_FILENO, "\r\n", 2);
}

void cmd_help(char *arg[])
{
    int i;

    for (i = 0; cmd[i].id && arg[1]; ++i)
    {
        if (strcmp(cmd[i].id, arg[1]) == 0)
        {
            write(STDOUT_FILENO, cmd[i].id, strlen(cmd[i].id));
            write(STDOUT_FILENO, " - ", 3);
            write(STDOUT_FILENO, cmd[i].info, strlen(cmd[i].info));
            write(STDOUT_FILENO, "\r\n\r\nUsage: ", 12);
            write(STDOUT_FILENO, cmd[i].id, strlen(cmd[i].id));
            write(STDOUT_FILENO, " ", 1);
            write(STDOUT_FILENO, cmd[i].usage, strlen(cmd[i].usage));
            write(STDOUT_FILENO, "\r\n", 2);
            //klog("%s - %s\r\n\r\nUsage: %s %s\r\n",
            //        cmd[i].id, cmd[i].info, cmd[i].id, cmd[i].usage);
            return;
        }
    }

    for (i = 0; cmd[i].id; ++i)
    {
        write(STDOUT_FILENO, cmd[i].id, strlen(cmd[i].id));
        write(STDOUT_FILENO, ": ", 2);
        write(STDOUT_FILENO, cmd[i].info, strlen(cmd[i].info));
        write(STDOUT_FILENO, "\r\n", 2);
        //klog("%s: %s\r\n", cmd[i].id, cmd[i].info);
    }
}

static void execute(char *input)
{
    char *arg[128];
    int j = 1;
    int i;
    int fd[] =
    {
        STDIN_FILENO,
        STDOUT_FILENO,
        STDERR_FILENO,
    };
    int com;

    for (i = 0; input[i] == ' '; ++i)
        continue;
    arg[0] = input + i;

    for (; input[i]; ++i)
    {
        if (input[i] == ' ' || input[i] == '\t')
        {
            for (; input[i] == ' ' || input[i] == '\t'; ++i)
                input[i] = 0;
            if (input[i] == '\'')
            {
                arg[j++] = input + ++i;
                for (; input[i] && input[i] != '\''; ++i)
                    continue;
                if (input[i] == 0)
                    break;
                input[i] = 0;
            }
            else
                arg[j++] = input + i;
        }
    }

    arg[j] = 0;

    for (i = 0; cmd[i].id; ++i)
    {
        if (strcmp(arg[0], cmd[i].id) == 0)
        {
            cmd[i].f(arg);
            return;
        }
    }

    com = exec(arg, fd);
    if (com == -1)
    {
        write(STDOUT_FILENO, arg[0], strlen(arg[0]));
        write(STDOUT_FILENO, ": Command not found\r\n", 21);
    }
}

static void command(char *input)
{
    int i;
    int j = 0;
    int quote = 0;

    if (!input[0])
        return;

    for (i = 0; input[i]; ++i)
    {
        for (; input[i] && (input[i] != ';' || quote); ++i)
        {
            if (input[i] == '\'')
                quote = !quote;
        }
        if (input[i] == 0)
            break;
        input[i] = 0;
        execute(input + j);
        j = i + 1;
    }
    execute(input + j);
}

void cmd_run(char *arg[])
{
    if (!arg[1])
        return;

    command(getenv(arg[1]));
}

static char recv_char()
{
    char c;

    while (read(STDIN_FILENO, &c, 1) < 1)
        continue;

    return c;
}

static int readline(void *buffer, size_t len)
{
    int i;
    char c;
    char *data = buffer;

    for (i = 0; i < len; ++i)
    {
        c = recv_char();
        if (c == 0x1b)
        {
            recv_char();
            recv_char();
            --i;
        }
        else if (c == '\b')
        {
            if (i > 0)
            {
                --i;
                write(STDOUT_FILENO, &c, 1);
                write(STDOUT_FILENO, " ", 1);
                write(STDOUT_FILENO, &c, 1);
            }
            --i;
        }
        else if (c != '\n' && c != '\r')
        {
            data[i] = c;
            write(STDOUT_FILENO, &c, 1);
        }
        else
            return i;
    }

    return i;
}

inline void prompt(const char *prompt, char *fmt, size_t len)
{
    write(STDOUT_FILENO, prompt, strlen(prompt));
    fmt[readline(fmt, len)] = 0;
}

void app_shell(void)
{
    char input[128];

    for (;;)
    {
        prompt("> ", input, sizeof (input) - 1);
        write(STDOUT_FILENO, "\r\n", 2);

        command(input);
    }
}
