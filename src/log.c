/*
 ** log.c for ATUCOS
 **
 ** Made by Pierre Surply
 ** <pierre.surply@gmail.com>
 **
 ** Started on  Thu Feb 13 18:33:01 2014 Pierre Surply
 ** Last update Tue Mar 18 12:13:18 2014 Pierre Surply
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include <avr32/io.h>
#include <gpio.h>
#include <usart.h>
#include <pm.h>
#include <power_clocks_lib.h>
#include <board.h>

#define LOG_TARGET_PBACLK_FREQ_HZ FOSC0
#define LOG_USART                 (&AVR32_USART0)
#define LOG_USART_RX_PIN          AVR32_USART0_RXD_0_0_PIN
#define LOG_USART_RX_FUNCTION     AVR32_USART0_RXD_0_0_FUNCTION
#define LOG_USART_TX_PIN          AVR32_USART0_TXD_0_0_PIN
#define LOG_USART_TX_FUNCTION     AVR32_USART0_TXD_0_0_FUNCTION
#define LOG_USART_CLOCK_MASK      AVR32_USART0_CLK_PBA
#define LOG_PDCA_CLOCK_HSB        AVR32_PDCA_CLK_HSB
#define LOG_PDCA_CLOCK_PB         AVR32_PDCA_CLK_PBA

void klog_init(void)
{
    static const gpio_map_t gpio_map =
    {
        {LOG_USART_RX_PIN, LOG_USART_RX_FUNCTION},
        {LOG_USART_TX_PIN, LOG_USART_TX_FUNCTION}
    };

    static const usart_options_t usart_options =
    {
        .baudrate     = 115200,
        .charlength   = 8,
        .paritytype   = USART_NO_PARITY,
        .stopbits     = USART_1_STOPBIT,
        .channelmode  = USART_NORMAL_CHMODE
    };

    gpio_enable_module(gpio_map,
            sizeof(gpio_map) / sizeof(gpio_map[0]));

    usart_init_rs232(LOG_USART, &usart_options, LOG_TARGET_PBACLK_FREQ_HZ);
}

#define BUFF_SIZE 1024

void klog(const char *fmt, ...)
{
    va_list vl;
    char buff[BUFF_SIZE];

    va_start(vl, fmt);
    vsnprintf(buff, BUFF_SIZE, fmt, vl);
    usart_write_line(LOG_USART, buff);
    va_end(vl);
}

void panic(const char *fmt)
{
    klog("\n*** Panic ***\r\n");
    klog(fmt);
    klog("\r\n");
    asm volatile ("breakpoint");
    for (;;)
        continue;
}
