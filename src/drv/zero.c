/*
** zero.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <atucos/log.h>
#include <atucos/drv/zero.h>

void zero_init(struct dev *dev)
{
    klog("[... ] Setting up zero");
    klog("\r[OK  ]\r\n");
}

int zero_read(struct dev *dev, void *buffer, size_t len)
{
    int i;
    char *s = buffer;
    for (i = 0; i < len; ++ i)
        s[i] = 0;
    return 0;
}

int zero_write(struct dev *dev, const void *buffer, size_t len)
{
    return 0;
}

int zero_ioctl(struct dev *dev, int op, void *arg)
{
    return 0;
}


struct drv drv_zero =
{
    .id = "zero",
    .init = zero_init,
    .read = zero_read,
    .write = zero_write,
    .ioctl = zero_ioctl
};
