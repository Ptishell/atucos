/*
** null.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <atucos/log.h>
#include <atucos/drv/null.h>

void null_init(struct dev *dev)
{
    klog("[... ] Setting up null");
    klog("\r[OK  ]\r\n");
}

int null_read(struct dev *dev, void *buffer, size_t len)
{
    return 0;
}

int null_write(struct dev *dev, const void *buffer, size_t len)
{
    return 0;
}

int null_ioctl(struct dev *dev, int op, void *arg)
{
    return 0;
}


struct drv drv_null =
{
    .id = "null",
    .init = null_init,
    .read = null_read,
    .write = null_write,
    .ioctl = null_ioctl
};
