/*
** leds.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <avr32/io.h>
#include <gpio.h>
#include <board.h>
#include <led.h>
#include <atucos/log.h>
#include <atucos/dev.h>
#include <atucos/drv/lcd.h>

void leds_init(struct dev *dev)
{
    klog("[... ] Setting up %s", dev->id);
    gpio_configure_pin(LED0_GPIO,GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    gpio_configure_pin(LED1_GPIO,GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    gpio_configure_pin(LED2_GPIO,GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    gpio_configure_pin(LED3_GPIO,GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    gpio_configure_pin(LED4_GPIO,GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    gpio_configure_pin(LED5_GPIO,GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    gpio_configure_pin(LED6_GPIO,GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    gpio_configure_pin(LED7_GPIO,GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    klog("\r[OK  ]\r\n");
}

static inline void leds_set_value(struct dev *dev, char val)
{
    LED_Display(val);
}

void leds_write(struct dev *dev, const void *buffer, size_t len)
{
    int i;
    const char *s = buffer;
    for (i = 0; i < len; ++i)
        leds_set_value(dev, s[i]);
}

struct drv drv_leds =
{
    .id = "leds",
    .init = leds_init,
    .read = NULL,
    .write = leds_write,
    .ioctl = NULL
};
