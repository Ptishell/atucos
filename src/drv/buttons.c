/*
** buttons.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <avr32/io.h>
#include <gpio.h>
#include <board.h>
#include <buttons.h>
#include <atucos/log.h>
#include <atucos/dev.h>
#include <atucos/drv/lcd.h>

void buttons_init(struct dev *dev)
{
    klog("[... ] Setting up %s", dev->id);
    gpio_configure_pin(GPIO_PUSH_BUTTON_0, GPIO_DIR_INPUT);
    gpio_configure_pin(GPIO_PUSH_BUTTON_1, GPIO_DIR_INPUT);
    gpio_configure_pin(GPIO_PUSH_BUTTON_2, GPIO_DIR_INPUT);
    klog("\r[OK  ]\r\n");
}

static inline char buttons_read_value(struct dev *dev)
{
    return (gpio_get_pin_value(GPIO_PUSH_BUTTON_0)
            | (gpio_get_pin_value(GPIO_PUSH_BUTTON_1) << 1)
            | (gpio_get_pin_value(GPIO_PUSH_BUTTON_2) << 2));
}

void buttons_read(struct dev *dev, void *buffer, size_t len)
{
    int i;
    char *s = buffer;
    for (i = 0; i < len; ++i)
        s[i] = buttons_read_value(dev);
}

struct drv drv_buttons =
{
    .id = "buttons",
    .init = buttons_init,
    .read = buttons_read,
    .write = NULL,
    .ioctl = NULL
};
