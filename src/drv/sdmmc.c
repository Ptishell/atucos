/*
** sdmmc.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <stdlib.h>
#include <board.h>
#include <gpio.h>
#include <spi.h>
#include <conf_sd_mmc_spi.h>
#include <sd_mmc_spi.h>
#include <atucos/log.h>
#include <atucos/dev.h>
#include <atucos/drv/sdmmc.h>

void sd_mmc_spi_read_multiple_sector_callback(const void *psector)
{
}

void sd_mmc_spi_write_multiple_sector_callback(void *psector)
{
}

void sdmmc_init(struct dev *dev)
{
    klog("[... ] Setting up %s", dev->id);
    static const gpio_map_t gpio_map =
    {
        {SD_MMC_SPI_SCK_PIN,  SD_MMC_SPI_SCK_FUNCTION },
        {SD_MMC_SPI_MISO_PIN, SD_MMC_SPI_MISO_FUNCTION},
        {SD_MMC_SPI_MOSI_PIN, SD_MMC_SPI_MOSI_FUNCTION},
        {SD_MMC_SPI_NPCS_PIN, SD_MMC_SPI_NPCS_FUNCTION}
    };

    spi_options_t spi_options =
    {
        .reg          = SD_MMC_SPI_NPCS,
        .baudrate     = SD_MMC_SPI_MASTER_SPEED,
        .bits         = SD_MMC_SPI_BITS,
        .spck_delay   = 0,
        .trans_delay  = 0,
        .stay_act     = 1,
        .spi_mode     = 0,
        .modfdis      = 1
    };

    gpio_enable_module(gpio_map,
            sizeof(gpio_map) / sizeof(gpio_map[0]));

    spi_initMaster(SD_MMC_SPI, &spi_options);
    spi_selectionMode(SD_MMC_SPI, 0, 0, 0);
    spi_enable(SD_MMC_SPI);
    sd_mmc_spi_init(spi_options, FOSC0);

    klog("\r[OK  ]\r\n");
    if (sd_mmc_spi_mem_check())
        klog("    SD Card detected\r\n");
}

struct drv drv_sdmmc =
{
    .id = "sdmmc",
    .init = sdmmc_init,
    .read = NULL,
    .write = NULL,
    .ioctl = NULL
};
