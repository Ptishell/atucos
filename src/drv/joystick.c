/*
** joystick.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <avr32/io.h>
#include <gpio.h>
#include <board.h>
#include <joystick.h>
#include <atucos/log.h>
#include <atucos/dev.h>
#include <atucos/drv/lcd.h>

void joystick_init(struct dev *dev)
{
    klog("[... ] Setting up %s", dev->id);
    gpio_configure_pin(GPIO_JOYSTICK_PUSH,GPIO_DIR_INPUT);
    gpio_configure_pin(GPIO_JOYSTICK_LEFT,GPIO_DIR_INPUT);
    gpio_configure_pin(GPIO_JOYSTICK_UP,GPIO_DIR_INPUT);
    gpio_configure_pin(GPIO_JOYSTICK_DOWN,GPIO_DIR_INPUT);
    klog("\r[OK  ]\r\n");
}

static inline char joystick_read_value(struct dev *dev)
{
    return (is_joystick_up()
            | (is_joystick_down() << 1)
            | (is_joystick_left() << 2)
            | (is_joystick_right() << 3)
            | (is_joystick_pressed() << 4));
}

void joystick_read(struct dev *dev, void *buffer, size_t len)
{
    int i;
    char *s = buffer;
    for (i = 0; i < len; ++i)
        s[i] = joystick_read_value(dev);
}

struct drv drv_joystick =
{
    .id = "joystick",
    .init = joystick_init,
    .read = joystick_read,
    .write = NULL,
    .ioctl = NULL
};
