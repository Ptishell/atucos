/*
** usart.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <avr32/io.h>
#include <usart.h>
#include <atucos/log.h>
#include <atucos/dev.h>
#include <atucos/drv/usart.h>

void usart_init(struct dev *dev);
int usart_read(struct dev *dev, void *buffer, size_t len);
int usart_write(struct dev *dev, const void *buffer, size_t len);
int usart_ioctl(struct dev *dev, int op, void *arg);

struct drv drv_usart =
{
    .id = "usart",
    .init = usart_init,
    .read = usart_read,
    .write = usart_write,
    .ioctl = usart_ioctl
};

void usart_init(struct dev *dev)
{
    klog("[... ] Setting up %s", dev->id);
    klog("\r[OK  ]\r\n");
}

int usart_read(struct dev *dev, void *buffer, size_t len)
{
    int i;
    int c;
    int ret;
    char *s = buffer;

    for (i = 0; i < len; ++i)
    {
        if (usart_read_char(dev->addr, &c) != USART_SUCCESS)
                return i;
        s[i] = c;
    }

    return i;
}

int usart_write(struct dev *dev, const void *buffer, size_t len)
{
    int i;
    const char *s = buffer;
    for (i = 0; i < len; ++i)
    {
        while (usart_write_char(dev->addr, s[i]) == USART_TX_BUSY)
            continue;
    }
    return len;
}

int usart_ioctl(struct dev *dev, int op, void *arg)
{
    return 0;
}
