/** random.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <stdlib.h>
#include <atucos/log.h>
#include <atucos/drv/random.h>

void random_init(struct dev *dev)
{
    klog("[... ] Setting up random");
    srand(0);
    klog("\r[OK  ]\r\n");
}

int random_read(struct dev *dev, void *buffer, size_t len)
{
    int i;
    char *s = buffer;
    for (i = 0; i < len; ++ i)
        s[i] = rand();
    return 0;
}

int random_write(struct dev *dev, const void *buffer, size_t len)
{
    return 0;
}

int random_ioctl(struct dev *dev, int op, void *arg)
{
    return 0;
}


struct drv drv_random =
{
    .id = "random",
    .init = random_init,
    .read = random_read,
    .write = random_write,
    .ioctl = random_ioctl
};
