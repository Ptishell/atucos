/*
** eth.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <avr32/io.h>
#include <gpio.h>
#include <conf_eth.h>
#include <macb.h>
#include <atucos/log.h>
#include <atucos/dev.h>
#include <atucos/drv/eth.h>

void eth_init(struct dev *dev)
{
    klog("[... ] Setting up %s", dev->id);

    static const gpio_map_t gpio_map =
    {
        {EXTPHY_MACB_MDC_PIN,     EXTPHY_MACB_MDC_FUNCTION   },
        {EXTPHY_MACB_MDIO_PIN,    EXTPHY_MACB_MDIO_FUNCTION  },
        {EXTPHY_MACB_RXD_0_PIN,   EXTPHY_MACB_RXD_0_FUNCTION },
        {EXTPHY_MACB_TXD_0_PIN,   EXTPHY_MACB_TXD_0_FUNCTION },
        {EXTPHY_MACB_RXD_1_PIN,   EXTPHY_MACB_RXD_1_FUNCTION },
        {EXTPHY_MACB_TXD_1_PIN,   EXTPHY_MACB_TXD_1_FUNCTION },
        {EXTPHY_MACB_TX_EN_PIN,   EXTPHY_MACB_TX_EN_FUNCTION },
        {EXTPHY_MACB_RX_ER_PIN,   EXTPHY_MACB_RX_ER_FUNCTION },
        {EXTPHY_MACB_RX_DV_PIN,   EXTPHY_MACB_RX_DV_FUNCTION },
        {EXTPHY_MACB_TX_CLK_PIN,  EXTPHY_MACB_TX_CLK_FUNCTION}
    };

    gpio_enable_module(gpio_map,
            sizeof(gpio_map) / sizeof(gpio_map[0]));

    /*if (!xMACBInit(&AVR32_MACB))
      {
      klog("\r[Fail]\n");
      return;
      }*/
    klog("\r[OK  ]\r\n");
}

int eth_read(struct dev *dev, void *buffer, size_t len)
{
    return 0;
}

int eth_write(struct dev *dev, const void *buffer, size_t len)
{
    return 0;
}

int eth_ioctl(struct dev *dev, int op, void *arg)
{
    return 0;
}

struct drv drv_eth =
{
    .id = "eth",
    .init = eth_init,
    .read = eth_read,
    .write = eth_write,
    .ioctl = eth_ioctl
};
