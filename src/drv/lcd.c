/*
** lcd.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <avr32/io.h>
#include <gpio.h>
#include <board.h>
#include <dip204.h>
#include <spi.h>
#include <atucos/log.h>
#include <atucos/dev.h>
#include <atucos/drv/lcd.h>

void lcd_init(struct dev *dev);
int lcd_read(struct dev *dev, void *buffer, size_t len);
int lcd_write(struct dev *dev, const void *buffer, size_t len);
int lcd_ioctl(struct dev *dev, int op, void *arg);

struct drv drv_lcd =
{
    .id = "lcd",
    .init = lcd_init,
    .read = lcd_read,
    .write = lcd_write,
    .ioctl = lcd_ioctl
};

void lcd_init(struct dev *dev)
{
    klog("[... ] Setting up %s", dev->id);
    static const gpio_map_t gpio_map =
    {
        {DIP204_SPI_SCK_PIN,  DIP204_SPI_SCK_FUNCTION },
        {DIP204_SPI_MISO_PIN, DIP204_SPI_MISO_FUNCTION},
        {DIP204_SPI_MOSI_PIN, DIP204_SPI_MOSI_FUNCTION},
        {DIP204_SPI_NPCS_PIN, DIP204_SPI_NPCS_FUNCTION},
    };

    static const spi_options_t spi_options =
    {
        .reg          = DIP204_SPI_NPCS,
        .baudrate     = 1000000,
        .bits         = 8,
        .spck_delay   = 0,
        .trans_delay  = 0,
        .stay_act     = 1,
        .spi_mode     = 0,
        .modfdis      = 1
    };

    gpio_enable_module(gpio_map,
            sizeof(gpio_map) / sizeof(gpio_map[0]));
    spi_initMaster(DIP204_SPI, &spi_options);
    spi_selectionMode(DIP204_SPI, 0, 0, 0);
    spi_enable(DIP204_SPI);
    spi_setupChipReg(DIP204_SPI, &spi_options, FOSC0);
    dip204_init(backlight_PWM, true);
    dip204_clear_display();
    klog("\r[OK  ]\r\n");
}

int lcd_read(struct dev *dev, void *buffer, size_t len)
{
    return 0;
}

int lcd_write(struct dev *dev, const void *buffer, size_t len)
{
    size_t i;
    const char *s = buffer;
    for (i = 0; i < len; ++i)
        dip204_write_data(s[i]);
    return len;
}

int lcd_ioctl(struct dev *dev, int op, void *arg)
{
    struct lcd_cursor_pos *pos = arg;
    switch (op)
    {
        case IOCTL_LCD_CLEAR:
            dip204_clear_display();
            break;
         case IOCTL_LCD_SHOW_CURSOR:
            dip204_show_cursor();
            break;
         case IOCTL_LCD_HIDE_CURSOR:
            dip204_hide_cursor();
            break;
         case IOCTL_LCD_SET_CURSOR_POS:
            dip204_set_cursor_position(pos->x, pos->y);
            break;
    }
    return 0;
}
