/*
** cpu.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Tue Mar 18 11:28:43 2014 Pierre Surply
** Last update Tue Mar 18 15:48:26 2014 Pierre Surply
*/

#include <stdlib.h>
#include <stdint.h>
#include <avr32/io.h>
#include <wdt.h>
#include <atucos/log.h>

void cmd_reset(char *argv[])
{
    wdt_opt_t wdt_opt =
    {
        .us_timeout_period = 1000000
    };

    klog("Bye !\n");
    wdt_enable(&wdt_opt);
    for (;;)
        continue;
}
