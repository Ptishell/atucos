/*
** sdram.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <avr32/io.h>
#include <sdramc.h>
#include <board.h>
#include <atucos/log.h>

void sdram_init(void)
{
    klog("[... ] Setting up SDRAM");
    sdramc_init(FOSC0);
    klog("\r[OK  ]\r\n");
}
