/*
** main.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Thu Feb 13 18:30:47 2014 Pierre Surply
** Last update Tue Mar 18 17:56:02 2014 Pierre Surply
*/

#include <avr32/io.h>
#include <pm.h>
#include <power_clocks_lib.h>
#include <board.h>
#include <wdt.h>
#include <intc.h>
#include <atucos/log.h>
#include <atucos/interrupt.h>
#include <atucos/scheduler.h>
#include <atucos/sdram.h>
#include <atucos/mem.h>
#include <atucos/env.h>
#include <atucos/vfs.h>
#include <atucos/process.h>
#include <atucos/atucos.h>
#include <atucos/drv.h>
#include <atucos/binfs.h>
#include <atucos/app.h>

void process_init(void)
{
    struct dev *dev = vfs_getinode(vfs_getroot(), "/dev/usart0")->content.dev;
    struct process *init_proc = process_create("shell", (uint32_t) app_shell,
            dev, dev, dev, dev, vfs_getroot());
    init_proc->builtin = 1;
    current_process = init_proc;
}

int main(void)
{
    wdt_disable();
    pcl_switch_to_osc(PCL_OSC0, FOSC0, OSC0_STARTUP);
    klog_init();
    klog(WELCOME);
    interrupt_init();
    sdram_init();
    mem_init();
    env_init();
    vfs_init();
    devfs_init();
    binfs_init();
    scheduler_init();

    process_init();

    Enable_global_interrupt();

    for (;;)
        continue;
    return 0;
}
