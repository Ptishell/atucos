/*
** syscall.c for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#include <stdlib.h>

int atucos_syscall(int syscall, void *arg1, void *arg2, void *arg3, void *arg4)
{
    asm volatile ("mov r8, %0" "\n\t"
                  "scall"
                  :
                  : "r" (syscall));
}

int write(int fd, const char *buff, size_t len)
{
    return atucos_syscall(0, (void *) fd, (void *) buff, (void *) len, NULL);
}

int read(int fd, char *buff, size_t len)
{
    return atucos_syscall(1, (void *) fd, buff, (void *) len, NULL);
}

int open(const char *path)
{
    return atucos_syscall(2, (char *) path, NULL, NULL, NULL);
}

int exec(char *cmd[], int fd[])
{
    return atucos_syscall(3, cmd, fd, NULL, NULL);
}

int _exit(int retval)
{
    atucos_syscall(4, (void *) retval, NULL, NULL, NULL);
    for (;;)
        continue;
    return 0;
}

int ioctl(int fd, int op, void *arg)
{
    return atucos_syscall(5, fd, op, arg, NULL);
}
