##
## Makefile for ATUCOS
##
## Made by Pierre Surply
## <pierre.surply@gmail.com>
##
## Started on  Sun Jan 12 11:26:21 2014 Pierre Surply
## Last update Tue Mar 18 15:16:56 2014 Pierre Surply
##

include config.mk

ELF	= atucos.elf
USER    = atucos.a

DRV     = intc\
	  pm\
	  cpu/mpu\
	  ebi/sdramc\
	  usart\
	  gpio\
	  flashc\
	  wdt\
	  pwm\
	  spi\
	  cpu/cycle_counter\
	  macb\
	  tc

CPPFLAGS = -march=$(ARCH) -mpart=$(PART) -DBOARD=$(BOARD)		\
	  -MMD -DARCH='$(ARCH)' -DPART='$(PART)' -DJTAG='$(JTAG)'	\
	  -I./include/\
	  $(foreach D, $(DRV), -I$(ASFDIR)/avr32/drivers/$(D)/)\
	  -I$(ASFDIR)/avr32/utils\
	  -I$(ASFDIR)/avr32/boards\
	  -I$(ASFDIR)/avr32/utils/preprocessor\
	  -I$(ASFDIR)/common/utils\
	  -I$(ASFDIR)/common/boards\
	  -I$(ASFDIR)/common/services/delay\
	  -I$(ASFDIR)/common/services/clock\
	  -I$(ASFDIR)/common/services/storage/ctrl_access/\
	  -I$(ASFDIR)/avr32/components/display/dip204/\
	  -I$(ASFDIR)/avr32/components/display/dip204/example/at32uc3a0512_evk1100\
	  -I$(ASFDIR)/avr32/components/memory/sd_mmc/sd_mmc_spi\
	  -I$(ASFDIR)/avr32/components/memory/sdram/\
	  -I$(ASFDIR)/avr32/components/ethernet_phy/dp83848/\
	  -I$(ASFDIR)/avr32/components/joystick/skrhabe010/\
	  -I$(ASFDIR)/avr32/boards/evk1100/

SRC	= main.c\
	  log.c\
	  env.c\
	  cpu.c\
	  vfs.c\
	  process.c\
	  interrupt.c\
	  syscall.c\
	  dev.c\
	  binfs.c\
	  sdram.c\
	  mem.c\
	  scheduler.c\
	  app/shell.c\
	  app/hello.c\
	  app/game.c\
	  drv/null.c\
	  drv/zero.c\
	  drv/random.c\
	  drv/sdmmc.c\
	  drv/lcd.c\
	  drv/eth.c\
	  drv/joystick.c\
	  drv/leds.c\
	  drv/buttons.c\
	  drv/usart.c

SRC	:= $(addprefix ./src/, $(SRC))

USERSRC = syscall.c

USERSRC := $(addprefix ./userland/, $(USERSRC))

USEROBJ = $(USERSRC:.c=.o)

ASFC	= $(foreach D, $(DRV), \
	    $(wildcard $(ASFDIR)/avr32/drivers/$(D)/$(D).c))\
	  $(ASFDIR)/avr32/drivers/pm/power_clocks_lib.c\
	  $(ASFDIR)/avr32/drivers/ebi/sdramc/sdramc.c\
	  $(ASFDIR)/avr32/drivers/cpu/mpu/mpu.c\
	  $(ASFDIR)/avr32/drivers/pm/pm_conf_clocks.c\
	  $(ASFDIR)/avr32/boards/evk1100/led.c\
	  $(ASFDIR)/avr32/components/display/dip204/dip204.c\
	  $(ASFDIR)/avr32/components/memory/sd_mmc/sd_mmc_spi/sd_mmc_spi_mem.c\
	  $(ASFDIR)/avr32/components/memory/sd_mmc/sd_mmc_spi/sd_mmc_spi.c\
	  $(ASFDIR)/common/services/clock/uc3a0_a1/sysclk.c\
	  $(ASFDIR)/common/services/storage/ctrl_access/ctrl_access.c

ASFS	= src/exception.S

OBJ	= $(ASFS:.S=.o) $(ASFC:.c=.o) $(SRC:.c=.o)
DEPS	= $(ASFC:.c=.d) $(SRC:.c=.d)

all:: $(ELF)

$(ELF): $(USER) $(OBJ)
	$(CC) $(CPPFLAGS) $(OBJ) $(USER) -o $@
	$(SIZE) --radix=16 --format=sysv $@

$(USER): $(USEROBJ)
	$(AR) cr $@ $^

chiperase::
	$(PROG) chiperase

program:: $(ELF) chiperase
	sleep 2
	$(PROG) program $(FLASH) -cxtal -e -v $<

db::
	$(GDBPROX) -e $(JTAG) $(FLASH) -a ":4242"

gdb:: $(ELF)
	$(GDB) $<

clean::
	$(RM) $(ELF) $(USER) $(OBJ) $(DEPS)

distclean:: clean
	$(RM) config.mk

-include $(DEPS)
