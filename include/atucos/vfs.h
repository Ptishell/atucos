 /*
 ** vfs.h for ATUCOS
 **
 ** Made by Pierre Surply
 ** <pierre.surply@gmail.com>
 **
 */

#ifndef VFS_H
#define VFS_H

#include <stdlib.h>
#include <atucos/dev.h>

#define VFS_ID_LEN 32

enum vfs_node_type
{
    VFS_DIR = 0,
    VFS_DEV,
    VFS_EXEC,
    VFS_FILE
};

union vfs_node_content
{
    struct vfs_dir *dir;
    struct dev *dev;
    void *data;
};

struct vfs_inode
{
    enum vfs_node_type type;
    union vfs_node_content content;
};

struct vfs_dir
{
    char id[VFS_ID_LEN];
    struct vfs_inode *inode;
    struct vfs_dir *next;
};

void vfs_init(void);
struct vfs_inode *vfs_getroot(void);
struct vfs_inode *vfs_getsub(struct vfs_dir *dir, const char *name,
        size_t len);
struct vfs_dir *vfs_create_dir(const char *id,
                               struct vfs_inode *inode,
                               struct vfs_dir *dir);
struct vfs_inode *vfs_create_inode(enum vfs_node_type type,
                                   struct vfs_inode *parent);
struct vfs_inode *vfs_getinode(struct vfs_inode *wd, const char *path);

#endif /* VFS_H */
