/*
** atucos.h for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Tue Mar 18 14:47:09 2014 Pierre Surply
** Last update Tue Mar 18 14:57:41 2014 Pierre Surply
*/

#ifndef ATUCOS_H
# define ATUCOS_H

# define VERSION        "dev (" __DATE__ " - " __TIME__ ")"
# define WELCOME        "*** ATUCOS ***\r\n"

#endif /* !ATUCOS_H */
