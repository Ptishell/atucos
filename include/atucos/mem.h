#ifndef MEM_H
#define MEM_H

#include <atucos/process.h>

void mem_init(void);
int mem_process(struct process *proc);
void mem_setmpu(struct process *proc);

#endif /* MEM_H */
