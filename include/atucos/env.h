/*
** env.h for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Mar 17 20:24:57 2014 Pierre Surply
** Last update Tue Mar 18 15:28:18 2014 Pierre Surply
*/

#ifndef ENV_H
# define ENV_H

# include <atucos/atucos.h>

# define DEFAULT_ENV                           \
        ENV_VAR("arch", ARCH);                 \
        ENV_VAR("part", PART);                 \
        ENV_VAR("jtag", JTAG);                 \
        ENV_VAR("version", VERSION)            \

void env_init(void);
char *getenv(const char *id);

#endif /* !ENV_H */
