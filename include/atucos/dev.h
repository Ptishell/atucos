#ifndef DEV_H
#define DEV_H

#include <atucos/vfs.h>
#include <atucos/dev.h>

#define DEV_NAME_LEN 32

struct dev
{
    char id[DEV_NAME_LEN];
    volatile void *addr;
    struct drv *drv;
};

void devfs_init(void);

#endif /* DEV_H */
