#ifndef APP_H
#define APP_H

void app_hello(void);
void app_shell(void);
void app_game(void);

#endif /* APP_H */
