#ifndef DRV_H
#define DRV_H

#include <stdlib.h>
#define DRV_ID_LEN 32

struct dev;

struct drv
{
    char id[DRV_ID_LEN];
    void (*init)(struct dev *dev);
    int (*read)(struct dev *dev, void *buffer, size_t len);
    int (*write)(struct dev *dev, const void *buffer, size_t len);
    int (*ioctl)(struct dev *dev, int op, void *arg);
};

#endif /* DRV_H */
