#ifndef PROCESS_H
#define PROCESS_H

#include <stdlib.h>
#include <stdint.h>
#include <atucos/dev.h>

#define PROC_NAME_LEN 32
#define NB_FRAMES     16

#define PROC_STATUS_NEW     0
#define PROC_STATUS_RUNNING 1
#define PROC_STATUS_EXITED  2

struct context
{
    uint32_t r7;
    uint32_t r6;
    uint32_t r5;
    uint32_t r4;
    uint32_t r3;
    uint32_t r2;
    uint32_t r1;
    uint32_t r0;
    uint32_t sr;
    uint32_t pc;
    uint32_t lr;
    uint32_t r12;
    uint32_t r11;
    uint32_t r10;
    uint32_t r9;
    uint32_t r8;
};

struct process
{
    char     id[PROC_NAME_LEN];
    uint8_t  status;
    uint8_t  builtin;
    uint32_t pc;
    uint32_t sp;
    size_t   fd_size;
    size_t   fd_maxsize;
    struct   dev **fd;
    struct   vfs_inode *wd;
    uint32_t region;
    uint16_t frames;
    struct   context context;
    struct   process *next;
    struct   process *prev;
};

struct process *process_create(const char *id,
                               uint32_t pc,
                               struct dev *stdin,
                               struct dev *stdout,
                               struct dev *stderr,
                               struct dev *stdcom,
                               struct vfs_inode *wd);

void process_destroy(void);

void proc_run(struct process *proc);
void process_set_context(struct process *proc, struct context *context);

extern struct process *current_process;

#endif /* PROCESS_H */
