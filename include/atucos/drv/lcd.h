#ifndef LCD_H
#define LCD_H

#include <stdint.h>
#include <atucos/drv.h>

#define IOCTL_LCD_CLEAR          0
#define IOCTL_LCD_SHOW_CURSOR    1
#define IOCTL_LCD_HIDE_CURSOR    2
#define IOCTL_LCD_SET_CURSOR_POS 3

struct lcd_cursor_pos
{
    uint8_t x;
    uint8_t y;
};

extern struct drv drv_lcd;

#endif /* LCD_H */
