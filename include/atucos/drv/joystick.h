#ifndef JOYSTICK_H
#define JOYSTICK_H

#include <atucos/drv.h>

#define IS_JOYSTICK_UP(X)       ((X) & 1)
#define IS_JOYSTICK_DOWN(X)     (((X) >> 1) & 1)
#define IS_JOYSTICK_LEFT(X)     (((X) >> 2) & 1)
#define IS_JOYSTICK_RIGHT(X)    (((X) >> 3) & 1)
#define IS_JOYSTICK_PRESSED(X)  (((X) >> 4) & 1)

extern struct drv drv_joystick;

#endif /* JOYSTICK_H */
