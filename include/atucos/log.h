/*
** log.h for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Thu Feb 13 18:41:46 2014 Pierre Surply
** Last update Sat Mar 15 13:46:06 2014 Pierre Surply
*/

#ifndef LOG_H
# define LOG_H

# include <stdlib.h>

void klog_init(void);
void klog(const char *fmt, ...);
void panic(const char *fmt);
void prompt(const char *prompt, char *fmt, size_t len);

#endif /* !LOG_H */
