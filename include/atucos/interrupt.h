/*
** interrupt.h for ATUCOS
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
*/

#ifndef INTERRUPT_H
# define INTERRUPT_H

void interrupt_init(void);

#endif /* !INTERRUPT_H */
