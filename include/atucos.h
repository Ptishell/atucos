#ifndef ATUCOS_H
#define ATUCOS_H

#include <stdlib.h>

#define STDIN_FILENO    0
#define STDOUT_FILENO   1
#define STDERR_FILENO   2
#define STDCOM_FILENO   3

int atucos_syscall(int syscall, void *arg1, void *arg2,
        void *arg3, void *arg4);


int write(int fd, const char *buff, size_t len);
int read(int fd, char *buff, size_t len);
int open(const char *path);
int exec(const char *path, int fd[]);
int _exit(int retval);
int ioctl(int fd, int op, void *arg);

#endif /* ATUCOS_H */
